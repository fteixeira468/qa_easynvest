@swapi_api
Feature: The Star Wars API
  As an application
  I want to send requests to the Star Wars API
  To know more details about Star Wars (films, peoples, planets, etc)

  @find_by_index
  Scenario Outline: Find <data_type> by Index
    When I send a request to consult the "<data_type>" with index 1
    Then the API returns the information of the "<data_type>" with index 1

    Examples:
      | data_type |
      | People    |
      | Planets   |
