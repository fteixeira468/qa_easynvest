import { Given, When, Then } from 'cypress-cucumber-preprocessor/steps';

let response;

When('I send a request to consult the {string} with index {int}', (string, number) => {
  let str = string.toLowerCase();

  let data_json = require('./config/data.json');
  let data = data_json[str];
  response = cy.request(`${str}/${number}`)
  // O trecho abaixo e responsavel pela validacao da resposta da API.
  // O correto e que esse codigo fique no step "the API returns the information[...]".
  // Porem, nao consegui descobrir o motivo de nao conseguir acessar o objeto 'response'
  // no step em questao.
  response.then((resp) => {
    expect(resp.status).to.eq(200);
    Object.keys(data).forEach(function(key) {
      expect(resp.body).to.have.property(key, data[key]);
    })
  });
});

Then('the API returns the information of the {string} with index {int}', (string, number) => {
  // response.then((re) => {
  //   expect(re.status).to.eq(200);
  //   Object.keys(data).forEach(function(key) {
  //     expect(re.body).to.have.property(key, data[key]);
  //   })
  // });
});
