# **Projeto de Testes Automatizados**

Olá,

Este é um projeto para testes automatizados de APIs Rest utilizando o framework **_Cypress_**

## **Tecnologias Utilizadas**
* **JavaScript e NodeJS** (_para o desenvolvimento do script automatizado_)
* **Cucumber** (_para ler as especificações dos testes escritos em Gherkin_)
* **Cypress** (_para o envio das requisições e validação dos resultados_)


## **Ferramentas Necessárias**
**NodeJS (versão 10.15.3)** - <https://nodejs.org/en/download/>

**Git Client** - <https://git-scm.com/downloads>


## **Executando a Automação**
* Antes de tudo, faça a instalação das Ferramentas informadas anteriormente. Logo, abra o terminal e faça um clone do projeto:
```git clone https://bitbucket.org/fteixeira468/qa_easynvest.git```

* Ainda no terminal, instale as dependências - ```npm install``` (o _Cypress_ e demais bibliotecas serão instaladas)
* Após, execute ```npm run cypress:run``` para execução dos testes em modo _headless_. Executando ```npm run cypress:open```, a interface do _Cypress_ é aberta e os testes podem ser iniciados por lá.
